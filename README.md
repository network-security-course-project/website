# Sample Website for a Course Project

This repository contains a Django project, built specifically for a course project. The project lacks every security
measure possible, so I highly advise you against basing any real-world projects on this!
