from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect


def index(request):
    context = {'failed': False}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('authenticated')
        else:
            context['failed'] = True
    return render(request, 'index.html', context)


def log_out(request):
    logout(request)
    return redirect('index')


def authenticated(request):
    return render(request, 'authenticated.html')

