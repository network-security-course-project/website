from django.urls import path

from app import views

urlpatterns = [
    path('', views.index, name='index'),
    path('logout/', views.log_out, name='logout'),
    path('authenticated/', views.authenticated, name='authenticated'),
]
